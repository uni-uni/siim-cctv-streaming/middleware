#!/bin/bash

# HOW TO RUN
# example 
# ./run.sh NAME SRC_PORT DST_PORT

NAME=$1
SRC_PORT=$2
DST_PORT=$3

docker run --name $NAME \
    -p $SRC_PORT:3000 \
    -p $DST_PORT:1935 \
    -v nginx.conf:/etc/nginx/nginx.conf:ro \
    -d nginx:1.22@sha256:62accd5c832bf46871dfd604f86db86a8d2e0e9e4a376c4a05469718a56702d4